# Licenses

To all files in this directory, the following license applies, unless the file is listed below with a different license specified.

## CC-BY-NC-SA

![CC-BY-NC-SA logo](https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc-sa.png)

These files are licensed under the Creative Commons Attribution NonCommercial ShareAlike license. This means that you are allowed to copy and distribute the files as much as you want, and you're also allowed to change them. However, you only have these rights if you always attribute the creator of each file (see section 'Creators' below) and apply the same license to the version you produce. In addition, you are not allowed to use these files for commercial purposes. More details are available [here](https://creativecommons.org/licenses/by-nc-sa/4.0).

# Creators

All files in this directory have been created by Gjalt-Jorn Peters.

# Exceptions

There are presently no exceptions: all files in this directory are licensed underr the above license.